<?php

namespace Drupal\domain_simple_sitemap\Batch;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\simple_sitemap\Batch\Batch as SimpleSitemapBatch;

/**
 * Batch class.
 */
class Batch extends SimpleSitemapBatch {
}
